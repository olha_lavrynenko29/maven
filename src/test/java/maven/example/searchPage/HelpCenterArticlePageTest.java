package maven.example.searchPage;

import maven.example.BasePageTest;
import maven.pages.automationPractise.HelpCenterArticlePage;
import maven.pages.automationPractise.HelpCenterMainPage;
import maven.pages.automationPractise.HelpCenterSearchResultPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static maven.pages.automationPractise.HelpCenterArticlePage.FEEDBACK_FORM;
import static maven.pages.automationPractise.HelpCenterArticlePage.SUCCESS_POPUP;
import static utils.Waiters.*;
import static utils.Waiters.DEFAULT_TIME_SECOND;

public class HelpCenterArticlePageTest extends BasePageTest {
    HelpCenterMainPage helpCenterPage;
    HelpCenterSearchResultPage helpCenterSearchResulPage;
    HelpCenterArticlePage helpCenterArticlePage;

    SoftAssert asert=new SoftAssert();

    @BeforeMethod
    public void beforeMethod() {
        helpCenterPage = new HelpCenterMainPage(getDriver());
        helpCenterSearchResulPage = new HelpCenterSearchResultPage(getDriver());
        helpCenterArticlePage = new HelpCenterArticlePage(getDriver());
    }

    @Test
    public void contactFeedbackForm() {
        helpCenterArticlePage.openHelpArticlePage();
        helpCenterPage.cookiesPopupClose();
        helpCenterArticlePage.clickContactButton();
        explicitWait(getDriver(), DEFAULT_TIME_SECOND, FEEDBACK_FORM);
        helpCenterArticlePage.fillTheForm("olavrinenko26@gmail.com");
        helpCenterArticlePage.pressTheSendButton();
        asert.assertFalse(Boolean.parseBoolean(SUCCESS_POPUP),"pop-up is shown");
        asert.assertEquals(SUCCESS_POPUP,"pop-up is visible");
        asert.assertAll();
    }
}

