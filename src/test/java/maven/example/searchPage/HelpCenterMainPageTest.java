package maven.example.searchPage;

import maven.example.BasePageTest;
import maven.pages.automationPractise.HelpCenterArticlePage;
import maven.pages.automationPractise.HelpCenterMainPage;
import maven.pages.automationPractise.HelpCenterSearchResultPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static maven.pages.automationPractise.HelpCenterArticlePage.*;
import static maven.pages.automationPractise.HelpCenterMainPage.*;
import static maven.pages.automationPractise.HelpCenterSearchResultPage.SEARCH_URL;
import static utils.Waiters.*;


public class HelpCenterMainPageTest extends BasePageTest {
    HelpCenterMainPage helpCenterPage;
    HelpCenterSearchResultPage helpCenterSearchResulPage;
    HelpCenterArticlePage helpCenterArticlePage;

    SoftAssert asert=new SoftAssert();

    @BeforeMethod
    public void beforeMethod() {
        helpCenterPage = new HelpCenterMainPage(getDriver());
        helpCenterSearchResulPage = new HelpCenterSearchResultPage(getDriver());
        helpCenterArticlePage = new HelpCenterArticlePage(getDriver());

    }

    @DataProvider(name = "for-help-urls")
    public Object[][] fordifferentSites() {
        return new Object[][]{{"https://help.auto-doc.fr/"}, {"https://help.autodoc.co.no/"}};
    }

    @DataProvider(name = "for-email-form")
    public Object[][] forDifferentEmail() {
        return new Object[][]{{"olavr.gmail.com"}, {"26@.com"}};
    }

    @Test(dataProvider = "for-email-form")
    public void autodocContactPage(String val) {
        helpCenterPage.openHelpPage();
        explicitWait(getDriver(), DEFAULT_TIME_SECOND, EMAIL_FORM);
        helpCenterPage.enterAndClickEmailForm(val);
        Assert.assertFalse(isElementPresent(getDriver(), DEFAULT_TIME_SECOND, By.xpath(THANKFULL_POPUP)), "popup element visible");
    }

    @Test
    public void topicsSearch() throws InterruptedException {
        helpCenterPage.openHelpPage();
        helpCenterPage.cookiesPopupClose();
        helpCenterPage.topicsSelect();
        Assert.assertEquals(getDriver().getCurrentUrl(), SEARCH_URL, "THe url is not current");
        helpCenterSearchResulPage.searchResultForArticle();
        explicitWaitURLToBePresent(getDriver(), DEFAULT_TIME_SECOND, ARTICLE_PAGE);
        asert.assertFalse(Boolean.parseBoolean(ARTICLE_PAGE), "assert true");
        asert.assertEquals(getDriver().getCurrentUrl(),ARTICLE_PAGE,"page is Article");
        asert.assertAll();
    }

    @Test(dataProvider = "for-help-urls")
    public void seeMoreButtonClick(String val) {
        helpCenterPage.openHelpPageforDataProvider(val);
        explicitWait(getDriver(), DEFAULT_TIME_SECOND, COOKIES_POPUP);
        helpCenterPage.cookiesPopupClose();
        helpCenterPage.firstArticleinTheTopClick();
        helpCenterPage.seeMoreClick();
        Assert.assertTrue(isElementPresent(getDriver(), DEFAULT_TIME_SECOND, By.xpath(CONTACT_US_BUTTON)), "popup element visible");
    }
}
