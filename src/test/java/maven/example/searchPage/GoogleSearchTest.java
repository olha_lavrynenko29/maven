package maven.example.searchPage;

import maven.example.BasePageTest;
import maven.pages.automationPractise.AutodocContactPage;
import maven.pages.automationPractise.GoogleResultPage;
import maven.pages.automationPractise.GoogleSearchPage;
import maven.pages.automationPractise.HelpCenterMainPage;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static maven.pages.automationPractise.AutodocContactPage.SITE_FOOTER_LINK;
import static maven.pages.automationPractise.GoogleResultPage.URL_AUTODOC;
import static maven.pages.automationPractise.HelpCenterMainPage.URL_HELP_CENTER;
import static utils.Waiters.*;

public class GoogleSearchTest extends BasePageTest {

    GoogleSearchPage googleSearchPage;
    GoogleResultPage googleResultPage;
    AutodocContactPage autodocContactPage;
    HelpCenterMainPage helpCenterPage;

    @BeforeMethod
    public void beforeMethod() {
        googleSearchPage = new GoogleSearchPage(getDriver());
        googleResultPage = new GoogleResultPage(getDriver());
        autodocContactPage = new AutodocContactPage(getDriver());
        helpCenterPage = new HelpCenterMainPage(getDriver());
    }

    @Test
    public void autodocContactPage() {
        googleSearchPage.openGooglePage();
        googleSearchPage.search("help center autodoc");
        googleResultPage.openUrlByName();
        explicitWaitURLToBePresent(getDriver(), DEFAULT_TIME_SECOND, URL_AUTODOC);
        Assert.assertEquals(getDriver().getCurrentUrl(), URL_AUTODOC);
        autodocContactPage.cookiesPopupClose();
        explicitWait(getDriver(), DEFAULT_TIME_SECOND, SITE_FOOTER_LINK);
        autodocContactPage.openUrlInFooter();
        explicitWaitURLToBePresent(getDriver(), DEFAULT_TIME_SECOND, URL_HELP_CENTER);
        Assert.assertEquals(getDriver().getCurrentUrl(), URL_HELP_CENTER, "");
    }
}