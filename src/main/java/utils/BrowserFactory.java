package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public enum BrowserFactory {

    CHROME {
        public WebDriver create() {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            return new ChromeDriver();
        }
    },
    EDGE {
        public WebDriver create() {
            System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/msedgedriver.exe");
            return new EdgeDriver();
        }
    },
    FIREFOX {
        public WebDriver create() {
            System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
            return new FirefoxDriver();
        }
    };

    public WebDriver create() {
        return null;
    }
}



