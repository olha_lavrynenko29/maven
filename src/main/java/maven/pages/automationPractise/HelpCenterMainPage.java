package maven.pages.automationPractise;

import maven.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HelpCenterMainPage extends BasePage {

    public HelpCenterMainPage(WebDriver driver) {
        super(driver);
    }

    public static final String URL_HELP_CENTER = "https://help.autodoc.co.uk/";
    public static final String COOKIES_POPUP = "//*[@class='close-notification']";
    public static final String EMAIL_FORM = "//*[@class='footer-subscribe__input']//*[@name='email']";
    public static final String CHECKBOX = "//*[@class='footer-subscribe__text']//label";
    public static final String SEND_BUTTON = "//*[@class='footer-subscribe__input']//button";
    public static final String THANKFULL_POPUP = "//*[@id='thank-subscribe___BV_modal_body_']";
    public static final String TOPICS_WORDS = "//*[@class='title-topics']//*";
    public static final String SEARCH_BUTTON = "//*[@class='wrap-button']//button";
    public static final String ARTICLE_BLOCK_TOP5 = "//*[@class='expansion-panel collapsed'][@aria-controls='question-0']";
    public static final String SEE_MORE_BUTTON = "#question-0 .text .wrap-edit-icon .show-more";

    @FindBy(xpath = COOKIES_POPUP)
    private WebElement coockiesPopup;

    @FindBy(xpath = EMAIL_FORM)
    private WebElement emailField;

    @FindBy(xpath = CHECKBOX)
    private WebElement checkbox;

    @FindBy(xpath = SEND_BUTTON)
    private WebElement sendButton;

    @FindBy(xpath = THANKFULL_POPUP)
    private WebElement thanksPopup;

    @FindBy(xpath = TOPICS_WORDS)
    private WebElement topicsWordsFirst;

    @FindBy(xpath = SEARCH_BUTTON)
    private WebElement searchForTopicsWord;

    @FindBy(xpath = ARTICLE_BLOCK_TOP5)
    private WebElement firstArticleInTheTop;

    @FindBy(css = SEE_MORE_BUTTON)
    private WebElement seeMoreButtonClickMethod;

    public HelpCenterMainPage openHelpPageforDataProvider(String val) {
        driver.get(val);
        return this;
    }

    public HelpCenterMainPage openHelpPage() {
        driver.get(URL_HELP_CENTER);
        return this;
    }

    public HelpCenterMainPage cookiesPopupClose() {
        coockiesPopup.click();
        return this;
    }

    public HelpCenterMainPage enterAndClickEmailForm(String val) {
        emailField.sendKeys(val);
        checkbox.click();
        sendButton.submit();
        return this;
    }

    public HelpCenterMainPage topicsSelect() {
        topicsWordsFirst.click();
        return this;
    }

    public HelpCenterMainPage firstArticleinTheTopClick() {
        firstArticleInTheTop.click();
        return this;
    }

    public HelpCenterMainPage seeMoreClick() {
        seeMoreButtonClickMethod.click();
        return this;
    }
}


