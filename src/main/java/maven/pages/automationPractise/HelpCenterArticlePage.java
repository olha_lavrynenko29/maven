package maven.pages.automationPractise;

import maven.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HelpCenterArticlePage extends BasePage {

    public HelpCenterArticlePage(WebDriver driver) {
        super(driver);
    }

    public static final String ARTICLE_PAGE2 = "https://help.autodoc.co.uk/categories/106/94/291";
    public static final String ARTICLE_PAGE = "https://help.autodoc.co.uk/categories/13/44/310";
    public static final String CONTACT_US_BUTTON = "//*[@class='info-help__btn']//button";
    public static final String EMAIL_FIELD = "//*[@id='contact-modal']//*[@name='email']";
    public static final String NAME_FIELD = "//*[@id='contact-modal']//*[@name='name']";
    public static final String TEXT_FIELD = "//*[@id='textarea']";
    public static final String CONTACT_FORM_BUTTON = "//*[@id='contact-modal']//*[@class='btn btn-primary']";
    public static final String SUCCESS_POPUP = "//*[@id='thank___BV_modal_body_']";
    public static final String FEEDBACK_FORM = "//*[@id='contact-modal']";


    @FindBy(xpath = CONTACT_US_BUTTON)
    private WebElement contactUsButton;

    @FindBy(xpath = EMAIL_FIELD)
    private WebElement enterEmail;

    @FindBy(xpath = NAME_FIELD)
    private WebElement enterName;

    @FindBy(xpath = TEXT_FIELD)
    private WebElement enterText;

    @FindBy(xpath = CONTACT_FORM_BUTTON)
    private WebElement clickThePushButton;

    public HelpCenterArticlePage openHelpArticlePage() {
        driver.get(ARTICLE_PAGE);
        return this;
    }

    public Object clickContactButton() {
        contactUsButton.click();
        return this;
    }


    public HelpCenterArticlePage fillTheForm(String text) {
        enterEmail.sendKeys(text);
        enterName.sendKeys(text);
        enterText.sendKeys(text);
        return this;
    }

    public HelpCenterArticlePage pressTheSendButton() {
        clickThePushButton.click();
        return this;
    }
}
