package maven.pages.automationPractise;

import maven.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HelpCenterSearchResultPage extends BasePage {

    public HelpCenterSearchResultPage(WebDriver driver) {
        super(driver);
    }

    public static final String SEARCH_URL = "https://help.autodoc.co.uk/search?articles=app";
    public static final String SEARCH_RESULT_TO_THE_ARTICLE = "//*[@class='search-result__results']//*";

    @FindBy(xpath = SEARCH_RESULT_TO_THE_ARTICLE)
    private WebElement searchResultToTheArticle;

    public HelpCenterSearchResultPage searchResultForArticle() {
        searchResultToTheArticle.click();
        return new HelpCenterSearchResultPage(driver);
    }
}