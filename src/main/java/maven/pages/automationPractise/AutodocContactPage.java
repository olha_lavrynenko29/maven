package maven.pages.automationPractise;

import maven.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utils.Waiters.DEFAULT_TIME_SECOND;
import static utils.Waiters.explicitWait;

public class AutodocContactPage extends BasePage {

    public AutodocContactPage (WebDriver driver) {
        super(driver);
    }

    public static final String SITE_FOOTER_LINK = "//*[@class='link' and contains (text(),'Help Center')]";
    public static final String COOKIES_POPUP = "//div[@class='block-cookies__close']";

    @FindBy(xpath = COOKIES_POPUP)
    private WebElement coockiesPopup;

    @FindBy(xpath = SITE_FOOTER_LINK)
    private WebElement siteLink1;

    public AutodocContactPage cookiesPopupClose() {
        coockiesPopup.click();
        return this;
    }

    public HelpCenterMainPage openUrlInFooter() {
        explicitWait(driver, DEFAULT_TIME_SECOND,SITE_FOOTER_LINK);
        siteLink1.click();
        for (String tab : driver.getWindowHandles()) {
            driver.switchTo().window(tab);

        }
        return new HelpCenterMainPage(driver);
    }
}

